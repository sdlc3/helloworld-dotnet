﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HelloWorld.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloWorldController : ControllerBase
    {
        private readonly ILogger<HelloWorldController> _logger;

        public HelloWorldController(ILogger<HelloWorldController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<string> GetAsync()
        {
            var httpClient = HttpClientFactory.Create();
            var url = "https://github.com/devops2k18/mavenrepo/blob/master/pom.xml";
            HttpResponseMessage response = await httpClient.GetAsync(url);
            if(response.StatusCode == HttpStatusCode.OK){
                var content = response.Content;

                var data = await content.ReadAsStringAsync();
                Console.WriteLine(data);


            }
            
            return "Hello world";
        }
    }
}
