using HelloWorld.Controllers;
using System;
using Xunit;

namespace HelloWorldUnitTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var target = new HelloWorldController(null);
            Assert.Equal("Hello world", target.Get());
        }
    }
}
